<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<w:document xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
            xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
            xmlns:o="urn:schemas-microsoft-com:office:office"
            xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
            xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml"
            xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
            xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
            xmlns:w10="urn:schemas-microsoft-com:office:word"
            xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
            xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
            xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
            xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
            xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
            xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" mc:Ignorable="w14 wp14">
    <w:body>
        <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="00817940">
            <w:pPr>
                <w:pStyle w:val="1"/>
            </w:pPr>
            <w:r>
                <w:t>表格</w:t>
            </w:r>
        </w:p>
        <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="00817940">
            <w:pPr>
                <w:pStyle w:val="a3"/>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t>${userName}的成绩单</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblW w:w="0" w:type="auto"/>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="548DD4"/>
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="548DD4"/>
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="548DD4"/>
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="548DD4"/>
                    <w:insideH w:val="single" w:sz="4" w:space="0" w:color="548DD4"/>
                    <w:insideV w:val="single" w:sz="4" w:space="0" w:color="548DD4"/>
                </w:tblBorders>
                <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0"
                           w:noVBand="1"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="1046"/>
                <w:gridCol w:w="1047"/>
                <w:gridCol w:w="1276"/>
                <w:gridCol w:w="1559"/>
                <w:gridCol w:w="2977"/>
            </w:tblGrid>
            <w:tr w:rsidR="00817940" w:rsidRPr="00BC7DF8" w:rsidTr="009E4A17">
                <w:trPr>
                    <w:trHeight w:val="553"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1046" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:color w:val="FFFFFF"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>姓名</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2323" w:type="dxa"/>
                        <w:gridSpan w:val="2"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="005F7BB6" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="005F7BB6">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>${userName}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1559" w:type="dxa"/>
                        <w:vMerge w:val="restart"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>照片</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2977" w:type="dxa"/>
                        <w:vMerge w:val="restart"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:noProof/>
                            </w:rPr>
                            <w:drawing>
                                <wp:inline distT="0" distB="0" distL="0" distR="0">
                                    <wp:extent cx="1495425" cy="952500"/>
                                    <wp:effectExtent l="0" t="0" r="9525" b="0"/>
                                    <wp:docPr id="2" name="图片 2" descr="BingWallpaper-2017-04-04"/>
                                    <wp:cNvGraphicFramePr>
                                        <a:graphicFrameLocks
                                                xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"
                                                noChangeAspect="1"/>
                                    </wp:cNvGraphicFramePr>
                                    <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                        <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                            <pic:pic
                                                    xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                <pic:nvPicPr>
                                                    <pic:cNvPr id="0" name="Picture 1"
                                                               descr="BingWallpaper-2017-04-04"/>
                                                    <pic:cNvPicPr>
                                                        <a:picLocks noChangeAspect="1" noChangeArrowheads="1"/>
                                                    </pic:cNvPicPr>
                                                </pic:nvPicPr>
                                                <pic:blipFill>
                                                    <a:blip r:embed="rId6" cstate="print">
                                                        <a:extLst>
                                                            <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                                                <a14:useLocalDpi
                                                                        xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main"
                                                                        val="0"/>
                                                            </a:ext>
                                                        </a:extLst>
                                                    </a:blip>
                                                    <a:srcRect/>
                                                    <a:stretch>
                                                        <a:fillRect/>
                                                    </a:stretch>
                                                </pic:blipFill>
                                                <pic:spPr bwMode="auto">
                                                    <a:xfrm>
                                                        <a:off x="0" y="0"/>
                                                        <a:ext cx="1495425" cy="952500"/>
                                                    </a:xfrm>
                                                    <a:prstGeom prst="rect">
                                                        <a:avLst/>
                                                    </a:prstGeom>
                                                    <a:noFill/>
                                                    <a:ln>
                                                        <a:noFill/>
                                                    </a:ln>
                                                </pic:spPr>
                                            </pic:pic>
                                        </a:graphicData>
                                    </a:graphic>
                                </wp:inline>
                            </w:drawing>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="00817940" w:rsidRPr="00BC7DF8" w:rsidTr="009E4A17">
                <w:trPr>
                    <w:trHeight w:val="553"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1046" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>性别</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2323" w:type="dxa"/>
                        <w:gridSpan w:val="2"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>${sex}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1559" w:type="dxa"/>
                        <w:vMerge/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2977" w:type="dxa"/>
                        <w:vMerge/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="009E4A17"/>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="00817940" w:rsidRPr="00BC7DF8" w:rsidTr="009E4A17">
                <w:trPr>
                    <w:trHeight w:val="553"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1046" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>民族</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2323" w:type="dxa"/>
                        <w:gridSpan w:val="2"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>${nation}</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1559" w:type="dxa"/>
                        <w:vMerge/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2977" w:type="dxa"/>
                        <w:vMerge/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="009E4A17"/>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidTr="009E4A17">
                <w:trPr>
                    <w:trHeight w:val="553"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1046" w:type="dxa"/>
                        <w:vMerge w:val="restart"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:color w:val="FFFFFF"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>成绩</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1047" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="548DD4"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:color w:val="FFFFFF"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:color w:val="FFFFFF"/>
                            </w:rPr>
                            <w:t>序号</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2835" w:type="dxa"/>
                        <w:gridSpan w:val="2"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="548DD4"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:color w:val="FFFFFF"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:color w:val="FFFFFF"/>
                            </w:rPr>
                            <w:t>科目</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2977" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="548DD4"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:color w:val="FFFFFF"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:color w:val="FFFFFF"/>
                            </w:rPr>
                            <w:t>得分</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <#list scoreList as s>
                <w:tr w:rsidR="00817940" w:rsidTr="009E4A17">
                    <w:trPr>
                        <w:trHeight w:val="553"/>
                    </w:trPr>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="1046" w:type="dxa"/>
                            <w:vMerge/>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                            <w:pPr>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:b/>
                                    <w:bCs/>
                                </w:rPr>
                            </w:pPr>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="1047" w:type="dxa"/>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p w:rsidR="00817940" w:rsidRPr="001113A5" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                            <w:pPr>
                                <w:jc w:val="center"/>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:hint="eastAsia"/>
                                </w:rPr>
                                <w:t>${s.sort}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="2835" w:type="dxa"/>
                            <w:gridSpan w:val="2"/>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p w:rsidR="00817940" w:rsidRPr="001113A5" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                            <w:pPr>
                                <w:jc w:val="center"/>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:hint="eastAsia"/>
                                </w:rPr>
                                <w:t>${s.subject}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="2977" w:type="dxa"/>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p w:rsidR="00817940" w:rsidRPr="001113A5" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                            <w:pPr>
                                <w:jc w:val="center"/>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:hint="eastAsia"/>
                                </w:rPr>
                                <w:t>${s.score}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                </w:tr>
            </#list>
            <w:tr w:rsidR="00817940" w:rsidTr="009E4A17">
                <w:trPr>
                    <w:trHeight w:val="553"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1046" w:type="dxa"/>
                        <w:vMerge/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="right"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="3882" w:type="dxa"/>
                        <w:gridSpan w:val="3"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>总分</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2977" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="001113A5" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>320</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="00817940" w:rsidTr="009E4A17">
                <w:trPr>
                    <w:trHeight w:val="553"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2093" w:type="dxa"/>
                        <w:gridSpan w:val="2"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="0084236F" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0084236F">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>自我评价</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="5812" w:type="dxa"/>
                        <w:gridSpan w:val="3"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="00817940" w:rsidRPr="001113A5" w:rsidRDefault="00817940" w:rsidP="009E4A17">
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>勤学好问，乐于助人</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
        </w:tbl>
        <w:p w:rsidR="00817940" w:rsidRPr="001F6C8C" w:rsidRDefault="00817940" w:rsidP="00817940"/>
        <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="00817940">
            <w:pPr>
                <w:pStyle w:val="1"/>
            </w:pPr>
            <w:r>
                <w:t>文本</w:t>
            </w:r>
        </w:p>
        <w:p w:rsidR="00817940" w:rsidRPr="00041D27" w:rsidRDefault="00817940" w:rsidP="00817940">
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t>我的名字叫</w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t>张三</w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t>，我是</w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t>四</w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t>年级</w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t xml:space="preserve"> 5 </w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t>班的学生，我的班主任是</w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                </w:rPr>
                <w:t>李四。</w:t>
            </w:r>
        </w:p>
        <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="00817940">
            <w:pPr>
                <w:pStyle w:val="1"/>
            </w:pPr>
            <w:r>
                <w:lastRenderedPageBreak/>
                <w:t>图片</w:t>
            </w:r>
        </w:p>
        <w:p w:rsidR="00817940" w:rsidRPr="00477914" w:rsidRDefault="00817940" w:rsidP="00817940">
            <w:r>
                <w:rPr>
                    <w:noProof/>
                </w:rPr>
                <w:drawing>
                    <wp:inline distT="0" distB="0" distL="0" distR="0">
                        <wp:extent cx="4610100" cy="2876550"/>
                        <wp:effectExtent l="0" t="0" r="0" b="0"/>
                        <wp:docPr id="1" name="图片 1" descr="BingWallpaper-2017-03-20"/>
                        <wp:cNvGraphicFramePr>
                            <a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"
                                                 noChangeAspect="1"/>
                        </wp:cNvGraphicFramePr>
                        <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                            <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                    <pic:nvPicPr>
                                        <pic:cNvPr id="0" name="Picture 2" descr="BingWallpaper-2017-03-20"/>
                                        <pic:cNvPicPr>
                                            <a:picLocks noChangeAspect="1" noChangeArrowheads="1"/>
                                        </pic:cNvPicPr>
                                    </pic:nvPicPr>
                                    <pic:blipFill>
                                        <a:blip r:embed="rId7" cstate="print">
                                            <a:extLst>
                                                <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                                    <a14:useLocalDpi
                                                            xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main"
                                                            val="0"/>
                                                </a:ext>
                                            </a:extLst>
                                        </a:blip>
                                        <a:srcRect/>
                                        <a:stretch>
                                            <a:fillRect/>
                                        </a:stretch>
                                    </pic:blipFill>
                                    <pic:spPr bwMode="auto">
                                        <a:xfrm>
                                            <a:off x="0" y="0"/>
                                            <a:ext cx="4610100" cy="2876550"/>
                                        </a:xfrm>
                                        <a:prstGeom prst="rect">
                                            <a:avLst/>
                                        </a:prstGeom>
                                        <a:noFill/>
                                        <a:ln>
                                            <a:noFill/>
                                        </a:ln>
                                    </pic:spPr>
                                </pic:pic>
                            </a:graphicData>
                        </a:graphic>
                    </wp:inline>
                </w:drawing>
            </w:r>
        </w:p>
        <w:p w:rsidR="00817940" w:rsidRDefault="00817940" w:rsidP="00817940">
            <w:pPr>
                <w:pStyle w:val="1"/>
            </w:pPr>
            <w:r>
                <w:t>超链接</w:t>
            </w:r>
        </w:p>
        <w:p w:rsidR="00817940" w:rsidRPr="00477914" w:rsidRDefault="00817940" w:rsidP="00817940">
            <w:hyperlink r:id="rId8" w:history="1">
                <w:r>
                    <w:rPr>
                        <w:rStyle w:val="a4"/>
                    </w:rPr>
                    <w:t>https://tower.im/teams/7298/todos/76978/</w:t>
                </w:r>
            </w:hyperlink>
        </w:p>
        <w:p w:rsidR="00817940" w:rsidRPr="002B0C91" w:rsidRDefault="00817940" w:rsidP="00817940"/>
        <w:p w:rsidR="007217D2" w:rsidRPr="00817940" w:rsidRDefault="007217D2">
            <w:bookmarkStart w:id="0" w:name="_GoBack"/>
            <w:bookmarkEnd w:id="0"/>
        </w:p>
        <w:sectPr w:rsidR="007217D2" w:rsidRPr="00817940" w:rsidSect="002B0C91">
            <w:pgSz w:w="11906" w:h="16838"/>
            <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992"
                     w:gutter="0"/>
            <w:cols w:space="425"/>
            <w:docGrid w:type="lines" w:linePitch="312"/>
        </w:sectPr>
    </w:body>
</w:document>