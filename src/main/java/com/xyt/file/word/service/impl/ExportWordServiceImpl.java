package com.xyt.file.word.service.impl;

import com.xyt.file.word.bean.Word;
import com.xyt.file.word.service.ExportWordService;
import com.xyt.file.word.utils.DataUtils;
import com.xyt.file.word.utils.DocUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;

@Service
public class ExportWordServiceImpl implements ExportWordService {
    private static String charset = "UTF-8";

    @Override
    public void exportDocWord(HttpServletResponse response, String extName) {
        try {
            Word word = DataUtils.getWord(extName);
            if ("doc".equals(extName)) {
                DocUtils.createDoc(word);
            } else if ("docx".equals(extName)) {
                DocUtils.createDocx(word);
            }
            if (word != null) {
                System.out.println("--------------filePath = " + word.getDocFile().getAbsolutePath());
                response.setCharacterEncoding(charset);
                response.setHeader("Content-Disposition", "attachment;filename=" + word.getDocName());
                response.setContentType("file");

                BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(word.getDocFile()));
                BufferedOutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
                byte[] buffer = new byte[inputStream.available() + 1024];
                int num = 0;
                /**
                 * 将数据读取到缓冲区中，再将缓冲区中数据传输出去
                 */
                while ((num = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, num);
                }
                inputStream.close();
                outputStream.close();
                if (word.getDocFile().exists()) {
                    word.getDocFile().delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
