package com.xyt.file.word.utils;

import com.xyt.file.word.bean.Word;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;


/**
 * @author Administrator
 * word文档生成工具类
 */
public class DocUtils {
    private static int bufferSize = 1024;

    /**
     * @param word
     */
    public static void createDoc(Word word) {
        try {
            byte[] bytes = FreeMarkerUtils.getFreemarkerContentBytes(
                    word.getTemplatePath(), word.getTemplateName(), word.getData());
            File docFile = new File(word.getDocName());
            FileOutputStream fos = new FileOutputStream(docFile);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            bos.write(bytes);
            bos.flush();
            bos.close();
            word.setDocFile(docFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createDocx(Word word) {
        try {
            ByteArrayInputStream bis = FreeMarkerUtils.getFreemarkerContentInputStream(
                    word.getTemplatePath(), word.getTemplateName(), word.getData());
            File docxFile = new File(word.getDocName());
            FileOutputStream fos = new FileOutputStream(docxFile);
            File docxZipFile = new File(DocUtils.class.getResource(word.getZipName()).getPath());
            ZipFile zipFile = new ZipFile(docxZipFile);
            Enumeration<? extends ZipEntry> zipEntrys = zipFile.entries();
            ZipOutputStream zos = new ZipOutputStream(fos);
            //开始覆盖文档------------------
            int len = -1;
            byte[] buffer = new byte[bufferSize];
            while (zipEntrys.hasMoreElements()) {
                ZipEntry next = zipEntrys.nextElement();
                //System.out.println("--------------next = " + next);
                InputStream is = zipFile.getInputStream(next);
                //next.toString().indexOf("media") < 0 这一步把文件原本的图片给删掉了
                //我自己的模板是需要保留原图片的，所以这行代码注释掉
                //if (next.toString().indexOf("media") < 0) {
                    zos.putNextEntry(new ZipEntry(next.getName()));
                    //如果是word/document.xml由程序输入
                    if ("word/document.xml".equals(next.getName())) {
                        if (bis != null) {
                            while ((len = bis.read(buffer)) != -1) {
                                zos.write(buffer, 0, len);
                            }
                            bis.close();
                        }
                    } else {
                        while ((len = is.read(buffer)) != -1) {
                            zos.write(buffer, 0, len);
                        }
                        is.close();
                    }
                //}
            }
            zos.flush();
            zos.close();
            word.setDocFile(docxFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
