package com.xyt.file.word.utils;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.*;

public class FreeMarkerUtils {
    private static String charset = "UTF-8";
    private static int bufferCapacity = 1048576;

    public static Configuration getConfiguration(String templatePath) {
        //创建配置实例
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_30);
        //设置编码
        configuration.setDefaultEncoding(charset);
        configuration.setClassForTemplateLoading(FreeMarkerUtils.class, templatePath);
        return configuration;
    }

    public static ByteArrayOutputStream getFreemarkerContentOutputStream(String templatePath, String templateName, Object data) {
        ByteArrayOutputStream bos = null;
        try {
            //获取模板
            Template template = getConfiguration(templatePath).getTemplate(templateName);
            bos = new ByteArrayOutputStream(bufferCapacity);
            Writer out = new BufferedWriter(new OutputStreamWriter(bos, charset));
            //生成文件
            template.process(data, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bos;
    }

    public static ByteArrayInputStream getFreemarkerContentInputStream(String templatePath, String templateName, Object data) {
        ByteArrayInputStream bis = null;
        try {
            ByteArrayOutputStream bos = getFreemarkerContentOutputStream(templatePath, templateName, data);
            bis = new ByteArrayInputStream(bos.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bis;
    }

    public static byte[] getFreemarkerContentBytes(String templatePath, String templateName, Object data) {
        try {
            ByteArrayOutputStream bos = getFreemarkerContentOutputStream(templatePath, templateName, data);
            return bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
