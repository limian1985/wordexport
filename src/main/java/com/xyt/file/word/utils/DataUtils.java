package com.xyt.file.word.utils;

import com.xyt.file.word.bean.Report;
import com.xyt.file.word.bean.Score;
import com.xyt.file.word.bean.Word;

import java.util.ArrayList;
import java.util.List;

public class DataUtils {

    public static Report getData() {
        int capacity = 10;

        Report report = new Report();
        report.setUserName("张三丰");
        report.setSex("男");
        report.setNation("蒙古族");

        List<Score> scoreList = new ArrayList<>(capacity);

        Score score = new Score();
        score.setSort("1");
        score.setSubject("语文");
        score.setScore("90");
        scoreList.add(0, score);

        score = new Score();
        score.setSort("2");
        score.setSubject("数学");
        score.setScore("120");
        scoreList.add(1, score);

        score = new Score();
        score.setSort("3");
        score.setSubject("英语");
        score.setScore("110");
        scoreList.add(2, score);

        score = new Score();
        score.setSort("4");
        score.setSubject("物理");
        score.setScore("110");
        scoreList.add(3, score);

        score = new Score();
        score.setSort("5");
        score.setSubject("化学");
        score.setScore("110");
        scoreList.add(4, score);

        score = new Score();
        score.setSort("6");
        score.setSubject("生物");
        score.setScore("110");
        scoreList.add(5, score);

        score = new Score();
        score.setSort("7");
        score.setSubject("政治");
        score.setScore("110");
        scoreList.add(6, score);

        score = new Score();
        score.setSort("8");
        score.setSubject("历史");
        score.setScore("110");
        scoreList.add(7, score);

        score = new Score();
        score.setSort("9");
        score.setSubject("地理");
        score.setScore("110");
        scoreList.add(8, score);

        report.setScoreList(scoreList);
        report.setTotalScore("320");
        report.setSelfJudge("勤学好问，乐于助人");
        return report;
    }

    public static Word getWord(String extName) {
        if (extName == null || "".equals(extName.trim())) {
            extName = "doc";
        }
        Word word = new Word();
        word.setData(DataUtils.getData());
        word.setDocPath("");
        word.setTemplatePath("/templates");
        if ("doc".equals(extName.toLowerCase())) {
            word.setDocName("test1-2.doc");
            word.setTemplateName("test1-2.ftl");
        } else if ("docx".equals(extName.toLowerCase())) {
            word.setDocName("test1-2.docx");
            word.setZipName(word.getTemplatePath() + "/test1-2.zip");
            word.setTemplateName("document.ftl");
        }
        return word;
    }

}
