package com.xyt.file.word.bean;

import lombok.Data;

import java.util.List;

@Data
public class Report {
    String userName;
    String sex;
    String nation;
    String totalScore;
    String selfJudge;
    List<Score> scoreList;
}
