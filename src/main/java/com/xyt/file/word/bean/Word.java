package com.xyt.file.word.bean;

import lombok.Data;

import java.io.File;

@Data
public class Word {
    Object data;
    String docName;
    String zipName;
    String docPath;
    String templateName;
    String templatePath;
    File docFile;
}
